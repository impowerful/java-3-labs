import java.util.Scanner;

public class Roulette{
    public static void main(String[]args){

        RouletteWheel wheel = new RouletteWheel();
        Scanner scan = new Scanner(System.in);
        int wallet = 100;
        boolean playagain = true;
        boolean playtable = true;
        boolean spinagain = true;
        String response;

        while(playagain){
            System.out.println("Would you like to make a bet");
            System.out.println("Y for yes or N for no");
            response = scan.next();

            if(response.equals("Y") || response.equals("y")){
                System.out.println("What number would you like to bet on");
                int number = scan.nextInt();

                System.out.println("How much would you like to bet");
                System.out.println("Your Current Balance is: " + wallet);
                int bet = scan.nextInt();
                wallet = wallet-bet;

                wheel.spin();
                System.out.println("The table has rolled a " + wheel.getValue());

                if(number == wheel.getValue()){
                    bet = bet*2;
                    wallet = wallet + bet;
                    
                    System.out.println("You won Congradulations");
                    System.out.println("Yor new balance is " + wallet);

                    while(spinagain){
                        System.out.println("Would you like to play again Y for Yes or N for No");
                        response = scan.next();

                        if(response.equals("Y") || response.equals("y")){
                            System.out.println("Welcome to the next table");
                            break;
                        }

                        else if(response.equals("N") || response.equals("n")){
                            System.out.println("Thank you for playing hope to see you again");
                            spinagain = false;
                            break;
                        }
                    }
                }
                else{
                    System.out.println("You lost");
                    System.out.println("Yor new balance is " + wallet);

                    while(spinagain){
                        System.out.println("Would you like to play again Y for Yes or N for No");
                        response = scan.next();

                        if(response.equals("Y") || response.equals("y")){
                            System.out.println("Welcome to the next table");
                            break;
                        }
                        
                        else if(response.equals("N") || response.equals("n")){
                            System.out.println("Thank you for playing hope to see you again");
                            spinagain = false;
                            break;
                        }
                    }
                }

            }
            else if(response.equals("N") || response.equals("n")){
                while(playtable){
                    System.out.println("Would you like to play the next wheel");
                    response = scan.next();

                    if(response.equals("Y") || response.equals("y")){
                        System.out.println("Welcome to the next table");
                        break;
                    }

                    else if(response.equals("N") || response.equals("n")){
                        System.out.println("Thank you for playing hope to see you again");
                        playtable = false;
                        break;
                    }

                    else{
                        continue;
                    }
                }
                if(playtable ==  false){
                    playagain = false;
                }
                continue;
            }
            else{
                System.out.println("Incorrect input input Y or N");
                continue;
            }
        
        if(spinagain ==  false){
            playagain = false;
        }
        continue;
        }
    }
}