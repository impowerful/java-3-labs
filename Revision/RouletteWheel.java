import java.util.Random;

public class RouletteWheel{

    private Random random;
    private int lastspin;

    public RouletteWheel(){
        this.random = new Random();
        this.lastspin = 0;
    }

    public void spin(){
        lastspin = random.nextInt(37);
    }

    public int getValue(){
        return lastspin;
    }
}